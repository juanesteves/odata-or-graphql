﻿using Core.Entities;
using GraphQL.Types.Book;
using GraphQL.Types.Company;
using HotChocolate.Subscriptions;
using Infraestructure;

namespace GraphQL
{
     [GraphQLDescription("Represents the mutations available.")]
    public class Mutation
    {
        [UseDbContext(typeof(DataBaseContext))]
        [GraphQLDescription("Adds a Company.")]
        public async Task<AddCompanyPayload> AddCompanyAsync(
            AddCompanyInput input,
            [ScopedService] DataBaseContext context,
            [Service] ITopicEventSender eventSender,
            CancellationToken cancellationToken
            )
        {
            var company = new Company
            {
                Id = input.Id,
                Name = input.Name,
                Rating = input.Rating,
            };

            context.Companies.Add(company);
            await context.SaveChangesAsync(cancellationToken);

            await eventSender.SendAsync(nameof(Subscription.OnCompanyAdded), company, cancellationToken);

            return new AddCompanyPayload(company);
        }

        [UseDbContext(typeof(DataBaseContext))]
        [GraphQLDescription("Adds a Book.")]
        public async Task<AddBookPayload> AddBookAsync(AddBookInput input,
            [ScopedService] DataBaseContext context,
            [Service] ITopicEventSender eventSender,
            CancellationToken cancellationToken
            )
        {
            var book = new Book
            {
                Id = 2,
                ISBN = "063-6-920-02371-5",
                Title = "Enterprise Games",
                Author = "Michael Hugos",
                Price = 49.99m,
                Location = new Address { City = "Bellevue", Street = "Main ST" },
                Press = new Press
                {
                    Id = 2,
                    Name = "O'Reilly",
                    Category = Category.EBook,
                    Email = "o@gmail.com"
                }
            };

            context.Books.Add(book);
            await context.SaveChangesAsync();

            await eventSender.SendAsync(nameof(Subscription.OnBookAdded), book, cancellationToken);

            return new AddBookPayload(book);
        }
    }
}