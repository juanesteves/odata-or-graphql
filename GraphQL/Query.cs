﻿using Core.Entities;
using Infraestructure;

namespace GraphQL
{
    [GraphQLDescription("Represents the queries available.")]
    public class Query
    {
        private DataBaseContext _db;

        public Query(DataBaseContext context)
        {
            _db = context;
        }

        [UseDbContext(typeof(DataBaseContext))]
        [UseFiltering]
        [UseSorting]
        [GraphQLDescription("Gets the queryable platform.")]
        public IQueryable<Company> GetCompany([ScopedService] DataBaseContext context)
         {
            return _db.Companies;
            //return context.Companies;
        }

       
        [UseDbContext(typeof(DataBaseContext))]
        [UseFiltering]
        [UseSorting]
        [GraphQLDescription("Gets the queryable command.")]
        public IQueryable<Book> GetBooks([ScopedService] DataBaseContext context)
        {
            return context.Books;
        }
    }
}
