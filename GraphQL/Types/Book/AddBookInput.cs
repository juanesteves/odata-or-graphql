﻿namespace GraphQL.Types.Book
{ 
        public record AddBookInput(int Id, string Autor, string Title);
    
}