﻿namespace GraphQL.Types.Book
{
    public record AddBookPayload(Core.Entities.Book book);
}
