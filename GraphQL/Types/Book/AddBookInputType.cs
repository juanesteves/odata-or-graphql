﻿namespace GraphQL.Types.Book
{
    public class AddBookInputType : InputObjectType<AddBookInput>
    {
        protected override void Configure(IInputObjectTypeDescriptor<AddBookInput> descriptor)
        {
            descriptor.Description("Represents the input to add for a command.");

            descriptor
                .Field(c => c.Title)
                .Description("Represents the title.");
            descriptor
                .Field(c => c.Autor)
                .Description("Represents the autor.");
            descriptor
                .Field(c => c.Id)
                .Description("Represents the unique ID of the platform which the command belongs.");

            base.Configure(descriptor);
        }
    }
}
