﻿namespace GraphQL.Types.Company
{
        public record AddCompanyPayload(Core.Entities.Company company);
}
