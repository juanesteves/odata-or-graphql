﻿namespace GraphQL.Types.Company
{
    public class AddCompanyInputType : InputObjectType<AddCompanyInput>
    {
        protected override void Configure(IInputObjectTypeDescriptor<AddCompanyInput> descriptor)
        {
            descriptor.Description("Represents the input to add for a command.");

            descriptor
                .Field(c => c.Rating)
                .Description("Represents the company's rating.");
            descriptor
                .Field(c => c.Name)
                .Description("Represents the name.");
            descriptor
                .Field(c => c.Id)
                .Description("Represents the unique ID of the platform which the command belongs.");

            base.Configure(descriptor);
        }
    }
}