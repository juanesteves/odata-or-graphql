﻿namespace GraphQL.Types.Company
{
    public record AddCompanyInput(int Id, string Name, int Rating);

}