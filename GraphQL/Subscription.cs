﻿using Core.Entities;

namespace GraphQL
{
    [GraphQLDescription("Represents the queries available.")]
    public class Subscription
    {
       
        [Subscribe]
        [Topic]
        [GraphQLDescription("The subscription for added company.")]
        public Company OnCompanyAdded([EventMessage] Company company)
        {
            return company;
        }

        [Subscribe]
        [Topic]
        [GraphQLDescription("The subscription for added company.")]
        public Book OnBookAdded([EventMessage] Book book)
        {
            return book;
        }
    }
}

