# OData - GraphQL

Small project to compare an api built with oData and GraphQl


This is a simple implementation using OData v4 in .NET 6, and hotChocolate v 12.7


This demo don't require a database engine. This API use EF Core In-Memory.

You can validate if everything is working at the URL https://localhost:7147/swagger/index.html

You can run api with graphql at the URL https://localhost:7147/graphql
