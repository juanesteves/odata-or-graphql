﻿using Infraestructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;

namespace OData.Controllers
{
    public class CompaniesController : ODataController
    {
        private DataBaseContext _db;

        public CompaniesController(DataBaseContext context)
        {
            _db = context;
            if (context.Companies.Count() == 0)
            {
                foreach (var b in DataSourceCompany.GetCompanies())
                {
                    context.Companies.Add(b);
                }
                context.SaveChanges();
            }
        }


        // Return all companies
        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.Companies);
        }
    }
}
