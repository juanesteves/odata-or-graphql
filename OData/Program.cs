using GraphQL;
    using Infraestructure;
using Microsoft.AspNetCore.OData;
using Microsoft.EntityFrameworkCore;
using OData;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
    .AddOData(setup =>
        setup.AddRouteComponents(
                "odata",
                ODataExtension.GetEdmModel())
        .Filter()
        .Select()
        .Expand()
        .OrderBy()


    );

//GraphQL

builder.Services.AddGraphQLServer()
                .AddQueryType<Query>()
                .AddMutationType<Mutation>()
                .AddSubscriptionType<Subscription>()
                //TODO: Types implementation
                //.AddType<BookType>()
                //.AddType<CompanyType>()
                .AddProjections()
                .AddFiltering()
                .AddSorting();
                //TODO: Subscription 
                //.AddInMemorySubscriptions();



// In-Memory database
builder.Services.AddDbContext<DataBaseContext>((DbContextOptionsBuilder opt) => opt.UseInMemoryDatabase("DatabaseInMemory")); // new

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapGraphQL("/graphql");

app.MapControllers();

app.Run();
