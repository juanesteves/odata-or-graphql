﻿
using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infraestructure
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Press> Presses { get; set; }
        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Owned Entity Types https://bit.ly/3DHC9h8
            modelBuilder.Entity<Book>().OwnsOne(c => c.Location);
        }
    }
}