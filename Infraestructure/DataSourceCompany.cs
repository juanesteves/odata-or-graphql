﻿using Core.Entities;

namespace Infraestructure
{
    public class DataSourceCompany
    {
        private static IList<Company>? _companies { get; set; }

        public static IList<Company> GetCompanies()
        {
            if (_companies != null)
            {
                return _companies;
            }

            _companies = new List<Company>();

            // company #1
            Company company = new Company
            {
                Id = 1,
                Name = "Company 1",
                Rating = 10
                
            };
            _companies.Add(company);

            // company #2
            company = new Company
            {
                Id = 2,
                Name = "Company 2",
                Rating = 8

            };
            _companies.Add(company);

            company = new Company
            {
                Id = 3,
                Name = "Company 3",
                Rating = 4

            };
            _companies.Add(company);

            return _companies;
        }
    }
}
