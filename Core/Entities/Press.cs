﻿namespace Core.Entities
{
    public class Press
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public Category Category { get; set; }
    }
}
