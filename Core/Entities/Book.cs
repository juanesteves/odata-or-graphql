﻿namespace Core.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string ISBN { get; set; } = null!;
        public string Title { get; set; } = null!;
        public string Author { get; set; } = null!;
        public decimal Price { get; set; }
        public Address Location { get; set; } = null!;
        public Press Press { get; set; } = null!;
    }
}
