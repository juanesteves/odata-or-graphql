﻿namespace Core.Entities
{
    public class Address
    {
        public string City { get; set; } = null!;
        public string Street { get; set; } = null!;
    }
}
